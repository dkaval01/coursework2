package waterBodies;

/**
 * @author Domantas Kavaliauskas
 *         created on 1/5/2016
 */
public class MainRiverSegment {
    public static void main(String[] args) {
        RiverSegment first = new SourceSegment("Neris", 4.0, "a", 10.0);
        RiverSegment second = new SourceSegment("Nevezis", 5.5, "b", 10.0);
        RiverSegment third = new ConfluenceSegment("Nemunas", 2.0 , first, second, 10.0);

        RiverSegment eight = new SourceSegment("Minija", 6.0, "a", 10.0);
        RiverSegment nine = new SourceSegment("Merkys", 0.0, "a", 10.0);
        RiverSegment ten = new ConfluenceSegment("Nemunas2", 3.0, eight, nine, 10.0);

        RiverSegment eleven = new ConfluenceSegment("Vaiguva", 1.0, first, ten, 10.0);

        StillWaterBody fourth = new Sea("Baltic", 100.0, "Atlantic", 200.1);
        RiverNetwork five = new RiverNetwork(third, fourth);
        WaterBody[] six = {first, second, third, fourth, five};
        /**double longestPath = third.getLongestPath();
        System.out.print(longestPath); */

        System.out.println(third.calcTotalPower());
        System.out.println(first.calcTotalPower());
        System.out.println(second.calcTotalPower());
        System.out.println(fourth.calcTotalPower());
        System.out.println(five.calcTotalPower());
        System.out.println(WaterBody.mostPowerful(six));
        System.out.println(third.calcLongestPath());
        System.out.println(five.calcLongestPath());
        System.out.println(eleven.calcTotalPower());
        System.out.println(eleven.calcLongestPath());
        System.out.println(five.calcLongestPath());
        System.out.println(first.calcLongestPath());
        System.out.println(first.toString());
    }
}
