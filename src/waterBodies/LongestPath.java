package waterBodies;

/**
 * @author Domantas Kavaliauskas
 *         created on 1/4/2016
 */
public interface LongestPath {
    /**
     * A method to calculate longest path of a given structure.
     * @return double value of longest path.
     */
    double calcLongestPath();
}
