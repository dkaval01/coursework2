package waterBodies;

/**
 * @author Domantas Kavaliauskas
 *         created on 12/2/2015
 */
public abstract class WaterBody implements PowerOutput  {
    String name;
    public String getName() {
        return name;
    }

    /**
     * Returns 0 as a power of a water body if the method is not overwritten,
     * @return 0 value.
     */
    @Override
    public double calcTotalPower() {
        return 0;
    }

    /**
     * Returns a water body which generates most power from a given array,
     * @param waterBodies an array of water bodies,
     * @return waterBodies.WaterBody which generates most power.
     */
    public static WaterBody mostPowerful(WaterBody[] waterBodies) {
        if(waterBodies == null) {
            return null;
        }
        else {
            WaterBody body = waterBodies[0];
            for (int i = 1; i < waterBodies.length; i++) {
                if (waterBodies[i].calcTotalPower() > body.calcTotalPower()) {
                    body = waterBodies[i];
                }
            }
            return body;
        }
    }


    /**
     * Returns a String representation of waterBodies.WaterBody in format:
     * "waterBodies.WaterBody: name"
     * @return String representation of waterBodies.WaterBody.
     */
    @Override
    public String toString() {
        return "waterBodies.WaterBody: " + name + ".";
    }
}
