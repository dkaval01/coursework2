package waterBodies;

/**
 * @author Domantas Kavaliauskas
 *         created on 12/2/2015
 */
public class ConfluenceSegment extends RiverSegment {

    /**
     * Constructs a waterBodies.ConfluenceSegment by assigning the  name, left and right children, which are waterBodies.RiverSegment
     * Confluence segment is made of, length in kilometers and powerGenerated in kilowatts.
     * @param name a name value of waterBodies.ConfluenceSegment represented as a String,
     * @param length the length of the segment in kilometers, represented as a double,
     * @param leftChild a left tributary of the Confluence segment represented as a waterBodies.RiverSegment,
     * @param rightChild a right tributary of the Confluence segment represented as a waterBodies.RiverSegment,
     * @param powerGenerated an amount of power in kilowatts the waterBodies.ConfluenceSegment generates.
     */
    public ConfluenceSegment(String name, Double length, RiverSegment leftChild, RiverSegment rightChild, double powerGenerated) {
        this.leftChild = leftChild;
        this.rightChild = rightChild;
        this.length = length;
        this.name = name;
        this.powerGenerated = powerGenerated;
    }

    /**
     * Returns a String representation of waterBodies.ConfluenceSegment in format:
     * "waterBodies.ConfluenceSegment: name, leftChild: leftChild.name, rightChild: rightChild.name,
     * length: length, powerGenerated: powerGenerated."
     * @return String representation of waterBodies.ConfluenceSegment.
     */
    @Override
    public String toString() {
        return "waterBodies.RiverSegment:" + name +
                ", leftChild: " + leftChild.name + ", rightChild: " + rightChild.name +
                ", length: " + length + ", powerGenerated: " + powerGenerated + '.';
    }
}
