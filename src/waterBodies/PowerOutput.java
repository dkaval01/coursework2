package waterBodies;

/**
 * @author Domantas Kavaliauskas
 *         created on 12/2/2015
 */
public interface PowerOutput {
    /**
     * Returns a double value of power output,
     * @return double value of total power output.
     */
    double calcTotalPower();
}
