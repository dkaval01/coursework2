package waterBodies;

/**
 * @author Domantas Kavaliauskas
 *         created on 12/2/2015
 */
public class Lake extends StillWaterBody{
    private String location;

    /**
     * Constructs a waterBodies.Lake by assigning name, area and location attributes,
     * @param name lake's name represented as a String,
     * @param area an area in square kilometers expressed as a double,
     * @param location a name of location where the lake is found.
     */
    public Lake(String name, double area, String location) {
        this.name = name;
        this.area = area;
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    /**
     * Returns a String representation of waterBodies.Lake in format:
     * "waterBodies.Lake: name, Area: area, Location: location."
     * @return String representation of waterBodies.Lake.
     */
    @Override
    public String toString() {
        return "waterBodies.Lake: " + name + ", Area: " + area +", Location: " + location + '.';
    }
}
