package waterBodies;

/**
 * @author Domantas Kavaliauskas
 *         created on 12/2/2015
 */
public abstract class StillWaterBody extends WaterBody {
    double area;

    public double getArea() {
        return area;
    }

    /**
     * Returns a String representation of waterBodies.StillWaterBody in format:
     * "waterBodies.StillWaterBody: name, Area: area."
     * @return String representation of waterBodies.StillWaterBody.
     */
    @Override
    public String toString() {
        return "waterBodies.StillWaterBody: " + name + ", Area: " + area +".";
    }

}
