package waterBodies;

/**
 * @author Domantas Kavaliauskas
 *         created on 12/2/2015
 */
public abstract class RiverSegment extends WaterBody implements LongestPath{
    double length;
    double powerGenerated;
    RiverSegment leftChild;
    RiverSegment rightChild;

    public RiverSegment getLeftChild() {
        return leftChild;
    }

    public double getLength() {
        return length;
    }

    public double getPowerGenerated() {
        return powerGenerated;
    }

    public RiverSegment getRightChild() {
        return rightChild;
    }

    /**
     * An auxiliary method to help calculating total power.
     * Returns total power of waterBodies.RiverSegment by adding power of it's children and itself;
     * @param aSegment waterBodies.RiverSegment which power is calculated of;
     * @return double value of total power of a waterBodies.RiverSegment.
     */
   private static double totalPower(RiverSegment aSegment) {
        if(aSegment.leftChild ==  null) {
            return aSegment.powerGenerated;
        }
        else {
            return totalPower(aSegment.leftChild) + totalPower(aSegment.rightChild) + aSegment.powerGenerated;
        }
    }

    /**
     * Returns total power of a waterBodies.RiverSegment, uses totalPower method.
     * @return double value of total power of river segments.
     */
    @Override
    public double calcTotalPower(){
        return totalPower(this);
    }

    /**
     * Returns a longest path by adding source length and longest path between it's children,
     * will return 0.0 if aSegment is null,
     * @param aSegment a river segment which longest path is calculated;
     * @return double value of longest path of the river segment.
     */
    private double longestPath(RiverSegment aSegment) {
        if(aSegment != null) {
            if(aSegment.leftChild != null) {
                if(longestPath(aSegment.leftChild) > longestPath(aSegment.rightChild)) {
                    return longestPath(aSegment.leftChild) + aSegment.length;
                }
                else {
                    return longestPath(aSegment.rightChild) + aSegment.length;
                }
            }
            else {
                return aSegment.length;
            }
        }
        else {
            return 0.0;
        }

    }
    /**
     * Returns longest path of a river segment by using longestPath method,
     * @return double value of longest path of the segment.
     */
    @Override
    public double calcLongestPath() {
        return longestPath(this);
    }

    /**
     * Returns a String representation of waterBodies.RiverSegment in format:
     * "waterBodies.RiverSegment: name, leftChild: leftChild.name, rightChild: rightChild.name,
     * length: length, powerGenerated: powerGenerated."
     * @return String representation of waterBodies.RiverSegment.
     */
    @Override
    public String toString() {
        return "waterBodies.RiverSegment:" + name +
                ", leftChild: " + leftChild.name + ", rightChild: " + rightChild.name +
                ", length: " + length + ", powerGenerated: " + powerGenerated + '.';
    }
}
