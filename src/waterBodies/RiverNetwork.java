package waterBodies;

/**
 * @author Domantas Kavaliauskas
 *         created on 12/2/2015
 */
public class RiverNetwork extends WaterBody implements LongestPath {
    private RiverSegment finalRiverSegment;
    private StillWaterBody flowsInto;

    public RiverNetwork(RiverSegment finalRiverSegment, StillWaterBody flowsInto) {
        this.finalRiverSegment = finalRiverSegment;
        this.flowsInto = flowsInto;
    }

    public RiverSegment getFinalRiverSegment() {
        return finalRiverSegment;
    }

    public StillWaterBody getFlowsInto() {
        return flowsInto;
    }
    /**
     * Returns total power of a waterBodies.RiverSegment, uses totalPower method.
     * @return double value of total power of river segments.
     */
    @Override
    public double calcTotalPower() {
        return finalRiverSegment.calcTotalPower() + flowsInto.calcTotalPower();
    }

    /**
     * Returns longest path of a river network by calculating longest path of finalRiverSegment,
     * @return double value of longest path the river network.
     */
    @Override
    public double calcLongestPath() {
        return finalRiverSegment.calcLongestPath();
    }

    /**
     * Returns a String representation of waterBodies.RiverNetwork in format:
     * "waterBodies.RiverNetwork: finalRiverSegment.name, FlowsInto: flowsInto."
     * @return String representation of waterBodies.StillWaterBody.
     */
    @Override
    public String toString() {
        return "waterBodies.RiverNetwork: " + finalRiverSegment.name + ", flowsInto: " + flowsInto + '.';
    }

}
