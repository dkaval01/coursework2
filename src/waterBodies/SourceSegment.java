package waterBodies;

/**
 * @author Domantas Kavaliauskas
 *         created on 12/2/2015
 */
public class SourceSegment extends RiverSegment {
    private String source;

    /**
     * Constructs a waterBodies.SourceSegment by assigning the  name, length in kilometers, source it flows from
     * and powerGenerated in kilowatts.
     * @param name a name value of waterBodies.SourceSegment represented as a String,
     * @param length the length of the segment in kilometers, represented as a double,
     * @param source a string value of segment source,
     * @param powerGenerated an amount of power in kilowatts the waterBodies.SourceSegment generates.
     */
    public SourceSegment(String name, Double length, String source, double powerGenerated) {
        this.name = name;
        this.length = length;
        this.source = source;
        this.powerGenerated = powerGenerated;
    }

    public String getSource() {
        return source;
    }

    /**
     * Returns a String representation of waterBodies.SourceSegment in format:
     * "waterBodies.SourceSegment: name, length: length, source: source, powerGenerated: powerGenerated."
     * @return String representation of waterBodies.SourceSegment.
     */
    @Override
    public String toString() {
        return "waterBodies.SourceSegment: " + name +", length: " + length +
                ", source: " + source + ", powerGenerated: " + powerGenerated + ".";
    }
}
